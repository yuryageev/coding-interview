DOM, Selectors

Given the HTML code in the jsbin https://jsbin.com/kufuvenepa/edit?html,js,output.

Implement button click handler function which:

1. Select h1 by id and change text to "Other Heading".
2. Select list item with text "Super" from unordered list by css class and change color to "white" and backgroud color to "green".
3. Select list item with text "Interesting" from unordered list by css class and make text bold.